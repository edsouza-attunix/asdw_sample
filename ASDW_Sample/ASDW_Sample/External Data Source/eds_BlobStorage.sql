CREATE EXTERNAL DATA SOURCE [eds_BlobStorage] WITH
(  
	TYPE = SHARD_MAP_MANAGER,
	LOCATION = 'myserver.database.windows.net',
	CREDENTIAL = [mycredential]	
)